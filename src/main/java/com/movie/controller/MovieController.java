package com.movie.controller;

import com.movie.dto.request.MovieRQ;
import com.movie.dto.response.MoviePageRS;
import com.movie.dto.response.MovieRS;
import com.movie.service.MovieSV;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/movie/")
@RequiredArgsConstructor
public class MovieController {

    private final MovieSV movieSV;

    @PostMapping
    public ResponseEntity<MovieRS> create(@Valid @RequestBody MovieRQ req) {
        return ResponseEntity
                .status(HttpStatus.CREATED)
                .body(movieSV.create(req));
    }

    @GetMapping
    public ResponseEntity<MoviePageRS> list(@RequestParam(defaultValue = "0") int page,
                                            @RequestParam(defaultValue = "5") int size) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(movieSV.list(page, size));
    }

    @GetMapping("{id}")
    public ResponseEntity<MovieRS> detail(@PathVariable Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(movieSV.detail(id));
    }

    @PutMapping("{id}")
    public ResponseEntity<MovieRS> update(@Valid @RequestBody MovieRQ req, @PathVariable Long id) {
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(movieSV.update(id, req));
    }

    @DeleteMapping("{id}")
    public ResponseEntity<MovieRS> delete(@PathVariable Long id) {
        movieSV.delete(id);
        return ResponseEntity
                .status(HttpStatus.OK)
                .body(null);
    }
}
