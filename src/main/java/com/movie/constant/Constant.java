package com.movie.constant;

public class Constant {
    public static final Integer ACTIVE = 1;
    public static final Integer DELETE = 2;
    public static final String INTERNAL_ERROR = "Opp! Someting went wrong";
    public static final String NO_CONTENT = "Data not found";
    public static final String MOVIE_EXIST = "This movie already exist";
}
