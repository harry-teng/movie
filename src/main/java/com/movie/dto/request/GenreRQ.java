package com.movie.dto.request;

import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

public class GenreRQ {

    private Long id;

    @NotEmpty(message = "Genre title can not be empty")
    @NotNull(message = "Genre title can not be null")
    @Length(max = 30, message = "Genre Title must be equal or less than 30")
    private String title;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
