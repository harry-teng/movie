package com.movie.dto.request;


import org.hibernate.validator.constraints.Length;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.List;

public class MovieRQ {

    @NotEmpty(message = "Title can not be empty")
    @NotNull(message = "Title can not be null")
    @Length(max = 30, message = "Title must be equal or less than 30")
    private String title;
    @NotNull(message = "Rating can not be null")
    @Max(value=5, message="Must be equal or less than 5")
    private Float rating;

    @Length(max = 150, message = "Title must be equal or less than 150")
    private String description;

    @Valid
    @NotEmpty(message = "Please provide genre")
    @NotNull(message = "Genre can not be null")
    private List<GenreRQ> genres;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Float getRating() {
        return rating;
    }

    public void setRating(Float rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<GenreRQ> getGenres() {
        return genres;
    }

    public void setGenres(List<GenreRQ> genres) {
        this.genres = genres;
    }

}
