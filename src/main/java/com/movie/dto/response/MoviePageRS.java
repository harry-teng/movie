package com.movie.dto.response;

import java.util.List;

public class MoviePageRS {
    private List<MovieRS> data;
    private PageRS page;

    public MoviePageRS(List<MovieRS> data, PageRS page) {
        this.data = data;
        this.page = page;
    }

    public List<MovieRS> getData() {
        return data;
    }

    public void setData(List<MovieRS> data) {
        this.data = data;
    }

    public PageRS getPage() {
        return page;
    }

    public void setPage(PageRS page) {
        this.page = page;
    }

}
