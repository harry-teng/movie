package com.movie.dto.response;

import java.util.List;

public class MovieRS {

    private Long id;
    private String title;
    private float rating;
    private String description;
    private Integer status;
    private List<GenreRS> genres;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public float getRating() {
        return rating;
    }

    public void setRating(float rating) {
        this.rating = rating;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<GenreRS> getGenres() {
        return genres;
    }

    public void setGenres(List<GenreRS> genres) {
        this.genres = genres;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

}
