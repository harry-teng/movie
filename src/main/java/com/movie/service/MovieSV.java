package com.movie.service;

import com.movie.dto.request.MovieRQ;
import com.movie.dto.response.MoviePageRS;
import com.movie.dto.response.MovieRS;


public interface MovieSV {
    public MoviePageRS list(int page, int size);
    public MovieRS create(MovieRQ req);
    public MovieRS update(Long id, MovieRQ req);
    public MovieRS detail(Long id);
    public void delete(Long id);
}
