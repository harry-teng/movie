package com.movie.service;

import com.movie.constant.Constant;
import com.movie.dto.request.MovieRQ;
import com.movie.dto.response.MoviePageRS;
import com.movie.dto.response.MovieRS;
import com.movie.dto.response.PageRS;
import com.movie.entity.MovieEntity;
import com.movie.exception.httpstatus.BadRequestError;
import com.movie.repository.MovieRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class MovieIMP implements MovieSV {

    private final MovieRepository repo;
    private final ModelMapper mapper;


    /**
     * @author Lyheang
     * @desc Getting a list of movie
     * @date 2/11/2022
     *
     * @return List<MovieRS>
     */
    @Override
    public MoviePageRS list(int page, int size) {
        try {
            Pageable paging = PageRequest.of(page, size);

            Page<MovieEntity> movies = repo.findByStatus(Constant.ACTIVE, paging);
            List<MovieRS> data = movies
                    .stream()
                    .map( lst -> mapper.map(lst, MovieRS.class) )
                    .collect(Collectors.toList());

            PageRS pageRS = new PageRS(movies.getNumber(), movies.getTotalElements(), movies.getTotalPages());

            return new MoviePageRS(data, pageRS);
        } catch (Exception ex) {
            throw ex;
        }

    }


    /**
     * @author Lyheang
     * @desc Create a movie
     * @date 2/11/2022
     *
     * @param req
     * @return MovieRS
     */
    @Override
    public MovieRS create(MovieRQ req) {
        MovieRS data;
        try {
            Boolean exist = repo.existsByTitleAndStatus(req.getTitle(), Constant.ACTIVE);
            if (exist) {
                throw new BadRequestError(Constant.MOVIE_EXIST);
            }
            MovieEntity movieEntity = mapper.map(req, MovieEntity.class);

            movieEntity.setTitle(req.getTitle().trim());

            repo.save(movieEntity);
            data = mapper.map(movieEntity, MovieRS.class);

            return data;
        } catch (Exception ex) {
            throw ex;
        }
    }


    /**
     * @author Lyheang
     * @desc Get a movie
     * @date 2/11/2022
     *
     * @param id
     * @return MovieRS
     */
    @Override
    public MovieRS detail(Long id) {
        try {
            MovieEntity movieEntity = repo.findByIdAndStatus(id, Constant.ACTIVE);
            if ( movieEntity != null ) {
                MovieRS data = mapper.map(movieEntity, MovieRS.class);
                return data;
            } else {
                return null;
            }

        } catch (Exception ex) {
            throw ex;
        }
    }


    /**
     * @author Lyheang
     * @desc Update movie
     * @date 2/11/2022
     *
     * @return List<MovieRS>
     */
    @Override
    public MovieRS update(Long id, MovieRQ req) {
        try {
            MovieEntity movieEntity = repo.findByIdAndStatus(id, Constant.ACTIVE);
            if (movieEntity == null) {
                throw new BadRequestError(Constant.NO_CONTENT);
            }

            Boolean exist = repo.existsByTitleAndStatus(req.getTitle(), Constant.ACTIVE);
            if (exist) {
                throw new BadRequestError(Constant.MOVIE_EXIST);
            }
            movieEntity = mapper.map(req, MovieEntity.class);
            movieEntity.setId(id);

            repo.save(movieEntity);

            MovieRS data = mapper.map(movieEntity, MovieRS.class);

            return data;
        } catch (Exception ex) {
            throw ex;
        }
    }


    /**
     * @author Lyheang
     * @desc Delete movies
     * @date 2/11/2022
     *
     * @return List<MovieRS>
     */
    @Override
    public void delete(Long id) {
        try {
            MovieEntity movie = repo
                    .findById(id)
                    .orElseThrow( () -> new BadRequestError(Constant.NO_CONTENT) );
            movie.setStatus(Constant.DELETE);
            repo.save(movie);
        } catch (Exception ex) {
            throw ex;
        }
    }

}
