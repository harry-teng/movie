package com.movie.exception.httpstatus;

public class NoContentError extends RuntimeException {
    private String message;
    private Object data;

    public NoContentError(String message) {
        this.message = message;
        this.data = data;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }
}
