package com.movie.exception.httpstatus;

public class BadRequestError extends RuntimeException {
    private String message;
    private Object data;

    public BadRequestError(String message) {
        this.message = message;
        this.data = data;
    }

    @Override
    public String getMessage() {
        return message;
    }

    public Object getData() {
        return data;
    }
}
