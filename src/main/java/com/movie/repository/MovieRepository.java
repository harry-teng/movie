package com.movie.repository;

import com.movie.entity.MovieEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface MovieRepository extends JpaRepository<MovieEntity, Long> {
    Page<MovieEntity> findByStatus(Integer status, Pageable pageable);
    MovieEntity findByIdAndStatus(Long id, Integer status);
    boolean existsByTitleAndStatus(String title, Integer status);
}
